﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="index" %>

<!DOCTYPE html>

<html>
<head>
    <link rel="stylesheet" href="icons/css/all.min.css">
    <link href="style/style.css" rel="stylesheet" />
    <script src="script/jquery.js"></script>
    <script>
        fo = [];
        v = "g1";
        
        function creatPager(psize) {
            $.ajax({
                url: "index.aspx/getSize",
                data: {},
                dataType: "json",
                contentType: "application/json",
                type: "post",
                success: function (res) {
                    wsize = res.d;
                    num = Math.ceil(wsize / psize);

                    l = $("<li>").addClass("left").html(">");
                    r = $("<li>").addClass("right").html("<");

                    $("#pager").empty();

                    $("#pager").append(r);

                    for (i = 0; i < num; i++) {
                        pa = $("<li>").html(i + 1).addClass("norm");

                        $("#pager").append(pa);
                    }

                    $("#pager").append(l);

                    $("#pager > li:eq(1)").addClass("select");

                },

                error: function (er) {
                    alert(er.responseText);
                },
            })
        }

        function fill(start, count, v) {
            $.ajax({
                url: "index.aspx/getAll",
                data: "{st:" + start + ", cu:" + count + "}",
                dataType: "json",
                contentType: "application/json",
                type: "post",
                success: function (res) {
                    fo = res.d;

                    $("#view").empty();
                    $("#view").removeClass().addClass(v);
                    for (i = 0; i < fo.length; i++) {

                        it = $("<div>").addClass("items");

                        img = $("<img>").attr({ src: "images/movie/" + fo[i].pic });
                        inf = $("<div>").addClass("info");

                        ban = $("<div>").addClass("banner").html("دوبله فارسی");

                        sp1 = $("<span>").addClass("name").html(fo[i].name);

                        sp2 = $("<span>").addClass("genre h4").html("ژانر");
                        sp3 = $("<span>").addClass("genre").html(fo[i].genre);

                        sp4 = $("<span>").addClass("lan h4").html("زبان");
                        sp5 = $("<span>").addClass("lan").html(fo[i].lan);

                        sp6 = $("<span>").addClass("tolid h4").html("سازنده");
                        sp7 = $("<span>").addClass("tolid").html(fo[i].maker);

                        like = $("<div>").addClass("like").html(fo[i].like);

                        dis = $("<div>").addClass("dis").html(fo[i].dis);

                        il = $("<i>").addClass("far fa-thumbs-up");

                        id = $("<i>").addClass("far fa-thumbs-down");

                        like.append(il);
                        dis.append(id);

                        if (fo[i].tar == true)
                            inf.append(ban);

                        inf.append(sp1).append(sp2).append(sp3).append(sp4).append(sp5).append(sp6).append(sp7).append(like).append(dis);

                        sho = $("<div>").addClass("short").html("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec ligula eu dui molestie ullamcorper eget eu ex. Duis congue ipsum quis urna molestie, at euismod nulla tempor. Praesent dapibus nisl eu diam efficitur, vel imperdiet lacus bibendum. Phasellus sollicitudin est eu ipsum feugiat, vel dignissim tortor elementum. Sed eleifend sem sed varius vestibulum. Sed quis ullamcorper turpis. Aliquam a lobortis magna. Integer facilisis ipsum urna, sed malesuada odio luctus at. Nullam in libero augue. Pellentesque egestas purus sit amet vulputate iaculis. In eget eros dapibus, molestie lectus eu, scelerisque nulla. Nullam ut imperdiet magna. Etiam volutpat quis lectus sed ultricies. Proin sed nulla augue. Aenean iaculis pulvinar lobortis.Praesent viverra risus et ultrices semper. Phasellus ultrices, elit non tempus vehicula, lectus tellus vestibulum nulla, nec laoreet enim neque et arcu. Curabitur orci quam, laoreet in sem nec, rhoncus pretium lorem. Ut libero dolor, rhoncus ut elit nec, mattis malesuada ligula. Mauris nec libero justo. Duis eu magna et nulla efficitur fringilla nec sed metus. Suspendisse porttitor convallis nibh at iaculis. Nullam vitae gravida purus, sed ultrices augue. Aenean elit dolor, finibus et lobortis at, consequat in risus. Maecenas rutrum scelerisque justo, ac tempor nulla ultricies eget. Aliquam rhoncus consectetur nunc quis ultricies. Aliquam erat volutpat. Fusce ultrices lorem id dolor aliquet, nec tincidunt mi mattis. Sed convallis turpis blandit lacus consectetur, at posuere justo porta.")

                        it.append(img).append(inf).append(sho);

                        $("#view").append(it);
                    }
                },
                error: function (er) {
                    alert(er.responseText);
                },
            })
        }

        $(document).ready(function () {
            creatPager(8);
            fill(0, 8, "g1");

            page = 1;

            $("#pager").on("click", ".norm", function () {

                $("#pager > li:not(.left):not(.right)").removeClass("select");
                $(this).addClass("select");
                $(".left").removeClass("select");
                $(".right").removeClass("select");

                page = $(this).html();
                count = $("#tedad > option:selected").html();
                start = (page - 1) * count;
                fill(start, count, v);
            });

            $("#pager").on("click", ".left", function () {

                pa = $("#pager > .select").html();
                co = parseInt(pa) + 2;

                if (co <= num + 1) {
                    $("#pager > .select").removeClass("select");
                    $("#pager > .norm:nth-child(" + co + ")").addClass("select");
                }

                page = $(".select").html();
                count = $("#tedad > option:selected").html();
                start = (page - 1) * count;
                fill(start, count, v);
            });

            $("#pager").on("click", ".right", function () {

                pa = $("#pager > .select").html();
                co = parseInt(pa);

                if (co > 1) {
                    $("#pager > .select").removeClass("select");
                    $("#pager > .norm:nth-child(" + co + ")").addClass("select");
                }

                page = $(".select").html();
                count = $("#tedad > option:selected").html();
                start = (page - 1) * count;
                fill(start, count, v);
            });


            $("#tedad").change(function () {
                ted = $("#tedad > option:selected").html();
                fill(0, ted, "g1");
                creatPager(ted);
                fill(0, ted, v);
            });

            $("#viewstyle > li").click(function () {
                $("#viewstyle > li").removeClass();
                $(this).addClass("sel");

                i = $(this).index() + 1;
                v = "g" + i;

                $("#view").removeClass().addClass(v);
            });
        });
    </script>
</head>
<body>
    <div id="header"></div>
    <div id="content">
        <div class="address">
            <a href="#">بهترین ها</a>
            /
            <a href="#">خارجی</a>
            /
            <a href="#">سریال</a>
        </div>
        <div class="grid">
            <div class="pager">
                <ul id="pager">
                    <li class="right"><</li>
                    <li class="select">1</li>
                    <li>2</li>
                    <li>3</li>
                    <li>4</li>
                    <li>5</li>
                    <li>6</li>
                    <li class="left">></li>
                </ul>
            </div>
            <div class="set">
                <ul id="viewstyle">
                    <li class="sel"></li>
                    <li></li>
                    <li></li>
                </ul>

                <select id="tedad">
                    <option>4</option>
                    <option selected>8</option>
                    <option>16</option>
                </select>
            </div>
            <div id="view" class="g3" runat="server">
                <%--<div class="items">
                    <img src="images/movie/01.jpg" />
                    <div class="info">
                        <div class="banner">دوبله فارسی</div>
                        <span class="name">بازی تاج و تخت</span>
                        <span class="genre h4">ژانر :</span>
                        <span class="ganre">فانتزی ، درام</span>
                        <span class="lan h4">زبان :</span>
                        <span class="lan">انگلیسی</span>
                        <span class="tolid h4">سازنده :</span>
                        <span class="tolid">HBO</span>
                        <div class="like">
                            <i class="far fa-thumbs-up"></i>
                            55
                        </div>
                        <div class="dis">
                            <i class="far fa-thumbs-down"></i>
                            12
                        </div>
                    </div>
                    <div class="short">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec ligula eu dui molestie ullamcorper eget eu ex. Duis congue ipsum quis urna molestie, at euismod nulla tempor. Praesent dapibus nisl eu diam efficitur, vel imperdiet lacus bibendum. Phasellus sollicitudin est eu ipsum feugiat, vel dignissim tortor elementum. Sed eleifend sem sed varius vestibulum. Sed quis ullamcorper turpis. Aliquam a lobortis magna. Integer facilisis ipsum urna, sed malesuada odio luctus at. Nullam in libero augue. Pellentesque egestas purus sit amet vulputate iaculis. In eget eros dapibus, molestie lectus eu, scelerisque nulla. Nullam ut imperdiet magna. Etiam volutpat quis lectus sed ultricies. Proin sed nulla augue. Aenean iaculis pulvinar lobortis.
                        Praesent viverra risus et ultrices semper. Phasellus ultrices, elit non tempus vehicula, lectus tellus vestibulum nulla, nec laoreet enim neque et arcu. Curabitur orci quam, laoreet in sem nec, rhoncus pretium lorem. Ut libero dolor, rhoncus ut elit nec, mattis malesuada ligula. Mauris nec libero justo. Duis eu magna et nulla efficitur fringilla nec sed metus. Suspendisse porttitor convallis nibh at iaculis. Nullam vitae gravida purus, sed ultrices augue. Aenean elit dolor, finibus et lobortis at, consequat in risus. Maecenas rutrum scelerisque justo, ac tempor nulla ultricies eget. Aliquam rhoncus consectetur nunc quis ultricies. Aliquam erat volutpat. Fusce ultrices lorem id dolor aliquet, nec tincidunt mi mattis. Sed convallis turpis blandit lacus consectetur, at posuere justo porta.
                    </div>
                </div>--%>
            </div>
        </div>
    </div>
    <div id="footer">
    </div>
    <script>
        movies = [{ name: "بازی تاج و تخت", genre: "فانتزی", lan: "انگلیسی ", maker: "HBO", tar: true, like: 55, dis: 12, img: "01.jpg" },
            { name: "ریک و مورتی", genre: "انیمیشن", lan: "انگلیسی ", maker: "جاستین رویلند", tar: false, like: 50, dis: 13, img: "02.jpg" },
            { name: "وایکینگ ها", genre: "تاریخی", lan: "انگلیسی ", maker: "MGM Television", tar: false, like: 53, dis: 10, img: "03.jpg" },
            { name: "افسار گسیخته", genre: "جنایی", lan: "انگلیسی ", maker: "سونی پیکچرز", tar: true, like: 40, dis: 20, img: "04.jpg" },
            { name: "دوستان", genre: "کمدی", lan: "انگلیسی ", maker: "NBC", tar: true, like: 99, dis: 0, img: "05.jpg" },
            { name: "کیمیا", genre: "چرتو پرت", lan: "فارسی", maker: "شبکه دو سیما", tar: true, like: 1, dis: 230, img: "06.jpg" },
            { name: "ستایش", genre: "چرتو پرت", lan: "فارسی", maker: "شبکه سه سیما", tar: true, like: 0, dis: 0, img: "07.jpg" },
            { name: "پیکی بلایندرز", genre: "تاریخی", lan: "انگلیسی", maker: "BBC", tar: false, like: 52, dis: 11, img: "08.jpg" },
            { name: "TEST", genre: "genre", lan: "language", maker: "maker", tar: false, like: 99, dis: 99, img: "08.jpg" },
        ]

        //for (i = 0; i < movies.length; i++) {
        //    it = document.createElement("div");
        //    it.className = "items";

        //    img = document.createElement("img");
        //    img.src = "images/movie/" + movies[i].img;

        //    inf = document.createElement("div");
        //    inf.className = "info";

        //    ban = document.createElement("div");
        //    ban.className = "banner";
        //    ban.innerHTML = "دوبله فارسی";

        //    sp1 = document.createElement("span");
        //    sp1.className = "name";
        //    sp1.innerHTML = movies[i].name;

        //    sp2 = document.createElement("span");
        //    sp2.className = "genre h4";
        //    sp2.innerHTML = "ژانر";
        //    sp3 = document.createElement("span");
        //    sp3.className = "genre";
        //    sp3.innerHTML = movies[i].genre;

        //    sp4 = document.createElement("span");
        //    sp4.className = "lan h4";
        //    sp4.innerHTML = "زبان";
        //    sp5 = document.createElement("span");
        //    sp5.className = "lan";
        //    sp5.innerHTML = movies[i].lan;

        //    sp6 = document.createElement("span");
        //    sp6.className = "tolid h4";
        //    sp6.innerHTML = "سازنده";
        //    sp7 = document.createElement("span");
        //    sp7.className = "tolid";
        //    sp7.innerHTML = movies[i].maker;

        //    like = document.createElement("div");
        //    like.className = "like";
        //    like.innerHTML = movies[i].like;

        //    dis = document.createElement("div");
        //    dis.className = "dis";
        //    dis.innerHTML = movies[i].dis;

        //    il = document.createElement("i");
        //    il.className = "far fa-thumbs-up";

        //    id = document.createElement("i");
        //    id.className = "far fa-thumbs-down";

        //    like.appendChild(il);
        //    dis.appendChild(id);

        //    if (movies[i].tar == true) {
        //        inf.appendChild(ban);
        //    }
        //    inf.appendChild(sp1);
        //    inf.appendChild(sp2);
        //    inf.appendChild(sp3);
        //    inf.appendChild(sp4);
        //    inf.appendChild(sp5);
        //    inf.appendChild(sp6);
        //    inf.appendChild(sp7);
        //    inf.appendChild(like);
        //    inf.appendChild(dis);

        //    it.appendChild(img);
        //    it.appendChild(inf);

        //    view.appendChild(it);
        //}
    </script>
</body>
</html>
