﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Services;

public partial class index : System.Web.UI.Page
{
    public class movie
    {
        public string name;
        public string genre;
        public string lan;
        public string maker;
        public bool tar;
        public int like;
        public int dis;
        public string pic;

        public movie(string na, string ge, string la, string ma, bool ta, int li, int di, string p)
        {
            name = na;
            genre = ge;
            lan = la;
            maker = ma;
            tar = ta;
            like = li;
            dis = di;
            pic = p;
        }
    }

    [WebMethod]
    public static int getSize()
    {
        return movies.Count;
    }

    [WebMethod]
    public static List<movie> getAll(string st, string cu)
    {
        int s = int.Parse(st);
        int c = int.Parse(cu);
        if (s + c > movies.Count)
        {
            c = movies.Count - s;
        }
        return movies.GetRange(s, c);
    }

    static List<movie> movies = new List<movie>();
    protected void Page_Load(object sender, EventArgs e)
    {
        movies.Clear();
        movies.Add(new movie("بازی تاج و تخت", "فانتزی", "انگلیسی", "HBO", true, 65, 23, "01.jpg"));
        movies.Add(new movie("ریک و مورتی", "انیمیشن", "انگلیسی", "جاستین رویلند", false, 50, 13, "02.jpg"));
        movies.Add(new movie("وایکینگ ها", "تاریخی", "انگلیسی", "MGM Television", false, 53, 10, "03.jpg"));
        movies.Add(new movie("افسار گسیخته", "جنایی", "انگلیسی", "سونی پیکچرز", true, 40, 20, "04.jpg"));
        movies.Add(new movie("دوستان", "کمدی", "انگلیسی", "NBC", true, 99, 0, "05.jpg"));
        movies.Add(new movie("کیمیا", "چرتو پرت", "فارسی", "شبکه دو سیما", true, 1, 230, "06.jpg"));
        movies.Add(new movie("ستایش", "چرتو پرت", "فارسی", "شبکه سه سیما", true, 0, 0, "07.jpg"));
        movies.Add(new movie("پیکی بلایندرز", "تاریخی", "انگلیسی", "BBC", false, 52, 11, "08.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "09.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "10.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "11.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "12.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "13.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "14.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "15.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "16.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "17.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "18.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "19.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "20.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "21.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "22.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "23.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "24.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "25.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "26.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "27.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "28.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "29.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "30.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "31.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "32.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "33.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "34.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "35.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "36.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "37.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "38.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "39.jpg"));
        movies.Add(new movie("TEST", "test", "test", "test", false, 99, 99, "40.jpg"));

        //for (int i = 0; i < movies.Count; i++)
        //{
        //    HtmlGenericControl it = new HtmlGenericControl("div");
        //    it.Attributes["class"] = "items";

        //    HtmlImage img = new HtmlImage();
        //    img.Src = "images/movie/" + movies[i].pic;

        //    HtmlGenericControl info = new HtmlGenericControl("div");
        //    info.Attributes["class"] = "info";

        //    HtmlGenericControl ban = new HtmlGenericControl("div");
        //    ban.Attributes["class"] = "banner";
        //    ban.InnerHtml = "دوبله فارسی";

        //    HtmlGenericControl sp1 = new HtmlGenericControl("span");
        //    sp1.Attributes["class"] = "name";
        //    sp1.InnerHtml = movies[i].name;

        //    HtmlGenericControl sp2 = new HtmlGenericControl("span");
        //    sp2.Attributes["class"] = "genre h4";
        //    sp2.InnerHtml = "ژانر";
        //    HtmlGenericControl sp3 = new HtmlGenericControl("span");
        //    sp3.Attributes["class"] = "genre";
        //    sp3.InnerHtml = movies[i].genre;

        //    HtmlGenericControl sp4 = new HtmlGenericControl("span");
        //    sp4.Attributes["class"] = "lan h4";
        //    sp4.InnerHtml = "زبان";
        //    HtmlGenericControl sp5 = new HtmlGenericControl("span");
        //    sp5.Attributes["class"] = "lan";
        //    sp5.InnerHtml = movies[i].lan;

        //    HtmlGenericControl sp6 = new HtmlGenericControl("span");
        //    sp6.Attributes["class"] = "tolid h4";
        //    sp6.InnerHtml = "سازنده";
        //    HtmlGenericControl sp7 = new HtmlGenericControl("span");
        //    sp7.Attributes["class"] = "tolid";
        //    sp7.InnerHtml = movies[i].maker;

        //    HtmlGenericControl like = new HtmlGenericControl("div");
        //    like.Attributes["class"] = "like";
        //    like.InnerHtml = movies[i].like.ToString();

        //    HtmlGenericControl dis = new HtmlGenericControl("div");
        //    dis.Attributes["class"] = "dis";
        //    dis.InnerHtml = movies[i].dis.ToString();

        //    HtmlGenericControl il = new HtmlGenericControl("i");
        //    il.Attributes["class"] = "far fa-thumbs-up";

        //    HtmlGenericControl id = new HtmlGenericControl("i");
        //    id.Attributes["class"] = "far fa-thumbs-up";

        //    like.Controls.Add(il);
        //    dis.Controls.Add(id);

        //    if (movies[i].tar == true)
        //    {
        //        info.Controls.Add(ban);
        //    }
        //    info.Controls.Add(sp1);
        //    info.Controls.Add(sp2);
        //    info.Controls.Add(sp3);
        //    info.Controls.Add(sp4);
        //    info.Controls.Add(sp5);
        //    info.Controls.Add(sp6);
        //    info.Controls.Add(sp7);
        //    info.Controls.Add(like);
        //    info.Controls.Add(dis);

        //    it.Controls.Add(img);
        //    it.Controls.Add(info);

        //    view.Controls.Add(it);
        //}
    }
}